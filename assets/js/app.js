var key="";
var dbg=console.log;
$(document).ready(function(){      
    $.req=function(url,data){
        var d=$.Deferred();
        $.ajax({
            url: url,
            type:"POST",
            data:data,
            headers:{'Authorization': 'Bearer '+key},
            success: function(op){
               d.resolve(op);
            },
            error:d.reject
        }); 
        return d;
    };
    key=localStorage.getItem("_lkey");   
    if(jwtverify(key)){
        $("body").load(view_url+"views/main.html");        
    }else{
        $("body").load(view_url+"views/login.html"); 
    }
});
function jwtverify(j){  
    var op=false;
    if(j){       
        var jwtspl = j.split('.')[1];  
        if(jwtspl){
            var base64 = jwtspl.replace('-', '+').replace('_', '/');
            op=JSON.parse(window.atob(base64));
            var current_time = Date.now() / 1000;
            if(op.exp<current_time){
                op=false;
            }
        }  
    }
    return op;
}