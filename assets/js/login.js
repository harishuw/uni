$(document).ready(function(){
    $('#login_form').on("submit",function(e){
        e.preventDefault();
        var user=$('#username').val();
        var pass=$('#password').val();
        $.when($.req(base_url+"login",{user:user,pass:pass})).then(function(res){
            if(res.status==200){
                key=res.key;
                localStorage.setItem("_lkey",res.key);
                $("body").load(view_url+"views/main.html");     
            }else{               
                $("#loginerr").show();
            }
        },function(e){
            $("#loginerr").show();
        });
    });
});