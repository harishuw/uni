$(document).ready(function(){
    $.fn.menu=function(){
        var pg=$(this).data('page');
        if(pg){
            $('#content_main').load(view_url+"views/"+pg);  
        }       
    };
    $('.sidebar .nav-link').click(function(){
        $('.sidebar .nav-link').removeClass('active');
        $(this).addClass('active');
        $(this).menu();
    });
    $('.sidebar .nav-link:first').menu();
});
function logout(){
    localStorage.clear();
    window.location.reload();
}