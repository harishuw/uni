var qpid=0;
$(document).ready(function(){
    $('#qp_form').on("submit",function(e){
        e.preventDefault();        
        $.when($.req(base_url+"saveqp",$('#qp_form').serializeArray())).then(function(data){
            $('#qp_form')[0].reset();
            $('#exampleModal').modal('hide');
            fillqp(data.qp);
        });
    });
    $('#course').change(function(){
        var s=$('#course option:selected').data("sem");
        if(s){
            var h="<option value=''>Select</option>";
            for(var i=1;i<=Number(s);i++){
                h+="<option data-sem='"+i+"' value='"+i+"'>Semester "+i+"</option>";
            }
            $('#class').html(h);
        }
    });
    getqpdata();
});
function getqpdata(){
   $.when($.req(base_url+"qpdata")).then(function(data){
        if(data.courses){
            var h="<option value=''>Select</option>";
            $.each(data.courses,function(i,e){
                h+="<option data-sem='"+e.c_semesters+"' value='"+e.c_id+"'>"+e.c_name+"</option>";
            });            
            $('#course').html(h);
        }
        if(data.qp){
            fillqp(data.qp);
        }
   });
}
function fillqp(qp){
    var ht="";
    $.each(qp,function(i,e){
        ht+="<tr>";
        ht+="<td>"+(i+1)+"</td>";
        ht+="<td>"+e.c_name+"</td>";
        ht+="<td>Semester "+e.qp_sem+"</td>";
        ht+="<td>"+e.qp_date+"</td>";
        ht+="<td><a class='btn btn-primary btn-sm' data-qpid='"+e.qp_id+"' onclick='$(this).menu();qpid="+e.qp_id+"' type='button'  data-page='questions.html'>View</a></td>";
        ht+="<td><a class='btn btn-primary btn-sm' data-qpid='"+e.qp_id+"' onclick='$(this).menu();qpid="+e.qp_id+"' type='button'  data-page='print.html' >Print</a></td>";
        ht+="</tr>";
    });
    $("#qp_table tbody").html(ht);
}