<?php
include_once("db.php");
include_once("model.php");
include_once("libraries/Jw.php");
class app{
    public $m;
    function __construct(){  
        $db=new db();      
        $this->m=new model($db->conn);       
        #$this->index();
    }
    function json($array){
        header('Content-Type: application/json');
        echo json_encode($array);
    }
    function index(){
        $r=$this->m->rows("Select * from users limit 1");       
        $this->json($r);     
    }
    function login(){
        $op=["status"=>0];
        $post=$this->getPost();
        if(!empty($post['user']) && !empty($post['pass'])){
            $r=$this->m->row("select * from users where u_name='".$post['user']."' AND u_password='".sha1($post['pass'])."'");              
            if(!empty($r)){
                $op['status']=200;
                $op['key']=jwt()->encode(["exp"=>time()+(3600*10),"uid"=>$r['u_id']]);
            }
        }
        $this->json($op);
    }
    function getPost() {
        $pdata=[];
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            foreach($_POST as $pk=>$pv){
                if(is_string($pv)){
                    $pv = trim(stripslashes(htmlspecialchars($pv)));
                }
                $pdata[$pk]=$pv;           
            }
        }       
        return $pdata;
    }
    function qpdata(){
        $this->auth();
        $r=$this->m->rows("Select * from courses");
        $this->json(["courses"=>$r,"qp"=>$this->getqp()]);
    }    
    function saveqp(){
        $this->auth();
        $data=$this->getPost();        
        $id=$this->m->insert("questionpaper",$data);       
        $this->json(["id"=>$id,"qp"=>$this->getqp()]);
    }
    function saveqs(){
        $this->auth();
        $data=$this->getPost();  
        $data['q_question']=$_POST['q_question'];      
        $id=$this->m->insert("questions",$data);       
        $this->json(["id"=>$id,"qs"=>$this->getqs($data['q_qp'])]);
    }
    function qsdata($id){
        $this->json(["qs"=>$this->getqs($id)]);
    }
    function print($id){
        $this->json(["qp"=>$this->m->row("Select * from questionpaper left join courses on qp_course=c_id where qp_id=$id"),"qs"=>$this->getqs($id)]);
    }
    function addCourse(){
        $this->auth();
        $data=$this->getPost();
        $id=$this->m->insert("courses",$data);       
        $this->json($this->m->rows("Select * from courses"));
    }
    function getqp(){
        $r=$this->m->rows("Select * from questionpaper left join courses on qp_course=c_id");
        return $r;
    }
    function delqp($id){
        $this->m->qry("delete from questions where q_id=$id LIMIT 1");        
    }
    function courses(){
        $r=$this->m->rows("Select * from courses");
        $this->json($r);
    }
    function getqs($id){
        $r=$this->m->rows("Select * from questions where q_qp=$id");
        return $r;
    }
    function auth(){
        $au=false;
        $ah=apache_request_headers();
        if(isset($ah['Authorization'])){
            if (preg_match('/Bearer\s(\S+)/', $ah['Authorization'], $matches)) {
                $jw=$matches[1];
                $au=jwt()->verify($jw);
            }
        }
        if(!$au){
            $this->json(["status"=>0,"error"=>"invalid request"]);
            exit;
        }
    }
    function excel(){
        include_once("libraries/excel.php");
    }
}
if (!function_exists('apache_request_headers')){
    function apache_request_headers(){
        if (!is_array($_SERVER)) {
            return array();
        }
        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

  
