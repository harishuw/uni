<?php
ini_set("display_errors",1);
include_once("app.php");
function url(){
    $segment=$_SERVER['SCRIPT_NAME'];
    if (preg_match("/\bindex.php\b/i",$_SERVER['REQUEST_URI'] )){
        $segment=explode($segment.'/',$_SERVER['REQUEST_URI']);
    }else{
        $segment=explode(preg_replace('@/+$@','',dirname($segment)).'/',$_SERVER['REQUEST_URI']);
    }
    $segment=explode('/',$segment[1]);
    return $segment;
}
$app=new app();
$url=url();
$fn=array_shift($url);
call_user_func_array([$app,$fn],$url);