<?php 
include_once 'libraries/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', '#');
$sheet->setCellValue('B1', 'Roll no');
$sheet->setCellValue('C1', 'Name');
$sheet->setCellValue('D1', 'Course');
$sheet->setCellValue('E1', 'Sem');

$writer = new Xlsx($spreadsheet);
$f='excel.xlsx';
$writer->save($f);
$filetype=filetype($f);
$filename=basename($f);
$fs=filesize($f);
$o=file_get_contents($f);
unlink($f);
header ("Content-Type: ".$filetype);
header ("Content-Length: ".$fs);
header ("Content-Disposition: attachment; filename=".$filename);
echo $o;