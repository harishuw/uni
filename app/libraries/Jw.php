<?php
include_once 'libraries/jwt/autoload.php';
use \Firebase\JWT\JWT;
class Jw{
    public $skey="Sdfdfsdsf";
    public $key="";
    public $type="HS256";
    
    function __construct(){
        $this->key=base64_encode($this->skey);
    }
    function encode($token){
       return JWT::encode($token, $this->key, $this->type);
    }
    function decode($jwt){
        $decoded = JWT::decode($jwt, $this->key, array($this->type));
        return (array) $decoded;
    }
    function verify($jwt){
        $v=false;
        try{ 
            $d= $this->decode($jwt);
            $v=true;
            $exp=$d['exp']??"";
            if($exp!="" && time()>$d['exp']){
                $v=false;
            }              
        }catch(Exception $e){
            $v=false;           
            $err[]=[
                "date"=>date("m/d/Y H:i:s A"),
                "error"=>$e,
                "jwt"=>$jwt
            ];           
        }
       return $v;
    }
}
function jwt($k="brevity",$t="HS256"){    
    $j=new Jw();
    $j->key=$k;
    $j->type=$t;
    return $j;
}