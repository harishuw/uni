<?php

class model{
    public  $con;
    public $errors;
    public $res;
    function __construct($c){
        $this->con=$c;
        $this->errors=[];
    }
    function qry($stmt){        
        try {
            $this->res=$this->con->exec($stmt);            
        } catch(Exception $e) {
            $this->errors[]="Error: " . $e->getMessage();
        }        
    }    
    function prep($stmt,$data=[]){
        try {
            $this->res=$this->con->prepare($stmt);
            $this->res->execute($data);           
        } catch(Exception $e) {
            $this->errors[]="Error: " . $e->getMessage();
        }    
    }
    function rows($stmt){
        $this->prep($stmt);
        $this->res->setFetchMode(PDO::FETCH_ASSOC);
        $result = $this->res->fetchAll();       
        return $result;
    }
    function row($stmt){
        $this->prep($stmt);
        $this->res->setFetchMode(PDO::FETCH_ASSOC);
        $result = $this->res->fetch();
        $this->res->closeCursor();       
        return $result;
    }
    function insert($table,$data){     
        $cols=implode(",",array_keys($data));
        $vals=implode(',',array_map(function($v,$k){
            return sprintf(":%s",$k); 
        },$data,array_keys($data)));
        $sql = "INSERT INTO $table ($cols) VALUES ($vals)";
        $this->prep($sql,$data);      
        return $this->con->lastInsertId(); 
    }
}