<?php
class db{
    public $conn;
    public $dbhost;
    public $dbuser;
    public $dbpass;
    public $dbname;
    public $dberrors;
    function __construct(){
        include_once("dbconfig.php");
        $this->dberrors=[];
        $this->connect();
    }
    function connect(){
        try {
            $this->conn = new PDO("mysql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser, $this->dbpass);            
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);           
        }catch(PDOException $e){
            $this->dberrors[]= "Connection failed: " . $e->getMessage();
        }
    }
    function __destruct(){
        $this->conn=null;    
    }
}